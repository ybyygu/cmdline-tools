#! /usr/bin/env python
# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::857a8087-c54d-42c2-b81c-c24e00e1fd32][857a8087-c54d-42c2-b81c-c24e00e1fd32]]
#===============================================================================#
#   DESCRIPTION:  create a directory hodling all .note files (updatedb and symlink)
#                 for convient searching by ag (the silver searcher)
#
#       OPTIONS:
#  REQUIREMENTS:  python, ag, locate
#         NOTES:  ---
#        AUTHOR:  Wenping Guo <ybyygu@gmail.com>
#       LICENCE:  GPL version 2 or upper
#       CREATED:  <2017-10-22 Sun 21:00>
#       UPDATED:  <2018-04-06 Fri 13:51>
#===============================================================================#
# 857a8087-c54d-42c2-b81c-c24e00e1fd32 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::2c7bedf7-20e7-43e4-b10d-be8466758dce][2c7bedf7-20e7-43e4-b10d-be8466758dce]]
import os.path
import subprocess

from hashlib import md5
from os import symlink
from os import getenv
from os import makedirs
from contextlib import suppress
# 2c7bedf7-20e7-43e4-b10d-be8466758dce ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::821857c9-dceb-4f28-b23a-3ddfeb156fc3][821857c9-dceb-4f28-b23a-3ddfeb156fc3]]
def locate_all_notes(limit=10):
    """get all .note files by invoking locate program (updatedb)"""

    cmdline = ['locate',
               '--null',
               '--existing',
               '--ignore-case',
               '--limit',
               '{}'.format(limit),
               '--regex',
               '{}/.*\.note(_archive)*$'.format(getenv('HOME'))]

    results = subprocess.check_output(cmdline)
    delim = '\x00'.encode()
    results = results.strip(delim)
    # to str object, not bytes
    # exclude symlinks
    files = [afile.decode() for afile in results.split(delim) if afile and not os.path.islink(afile)]
    return files
# 821857c9-dceb-4f28-b23a-3ddfeb156fc3 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::c0418a0e-a9a7-4ea4-8bbe-4c64962bb0af][c0418a0e-a9a7-4ea4-8bbe-4c64962bb0af]]
def hash_filename(filename):
    """
    Parameters
    ----------
    filename: a string representing a filename

    Return
    ------
    a unique string representing the filename
    """
    assert type(filename) == str, 'filename is not correcting decoded: {}'.format(filename)

    root, ext = os.path.splitext(filename)
    uid = md5(root.encode()).hexdigest()
    return "{}".format(uid)

def create_vdir_from_files(files, dirname):
    """create a virtual directory from a list of files

    Parameters
    ----------
    files: a list of files
    dirname: the directory to hold symlinks to original files
    """

    dir_common = os.path.commonprefix(files)

    # create database by symlinks
    for afile in files:
        # unique key and unique path
        ukey = hash_filename(afile)
        uroot = "{}/{}".format(ukey[:2], ukey[2:])

        # try to keep directory layout
        relpath = os.path.relpath(afile, dir_common)
        destfile = os.path.join(dirname, uroot, relpath)
        with suppress(FileExistsError):
            makedirs(os.path.dirname(destfile))
        symlink(afile, destfile)
# c0418a0e-a9a7-4ea4-8bbe-4c64962bb0af ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::d5df4871-7308-4e62-969f-6196ecc572f6][d5df4871-7308-4e62-969f-6196ecc572f6]]
def do_ag_search(pattern):
    # set directly for simplicity
    default_dir = "/home/ybyygu/.cache/notes"

    # follow symlinks
    cmdline = 'rg --ignore-case --follow'.split()
    cmdline.append(pattern)
    subprocess.call(cmdline, cwd=default_dir)
# d5df4871-7308-4e62-969f-6196ecc572f6 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::d7399ce6-f8f8-4033-bc06-2f5380f57af8][d7399ce6-f8f8-4033-bc06-2f5380f57af8]]
def main():
    import sys
    import argparse

    version = "%(prog)s v0.1"
    desc = "search all .note files using ag"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-v', '--version',
                        version=version,
                        action='version')
    parser.add_argument('-r', '--refresh',
                        dest='vdir',
                        action='store',
                        help='refresh .note file database under specific directory')
    parser.add_argument('-l', '--limit',
                        dest='limit',
                        default=1000,
                        action='store',
                        type=int,
                        help='the max number of entries from udpatedb')
    parser.add_argument('pattern',
                        action='store',
                        nargs='?',
                        default=None,
                        help='search pattern using ag')

    if len(sys.argv) == 1:
        parser.print_help()
        return

    cmdl = parser.parse_args()

    if cmdl.vdir:
        note_files = locate_all_notes(cmdl.limit)
        create_vdir_from_files(note_files, cmdl.vdir)
    elif cmdl.pattern:
        print('searching {} using ag:'.format(cmdl.pattern))
        do_ag_search(cmdl.pattern)

if __name__ == '__main__':
    main()
# d7399ce6-f8f8-4033-bc06-2f5380f57af8 ends here
