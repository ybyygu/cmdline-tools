#! /usr/bin/env bash
# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::787fb596-b93f-4ed0-957c-853984dfd949][787fb596-b93f-4ed0-957c-853984dfd949]]
#===============================================================================#
#   DESCRIPTION:  safe cleanup when calling external scripts
#
#       OPTIONS:  call script test.sh
#                 > runner.sh ./test.sh
#                 set a auto-kill time limit
#                 > runner.sh -t 5 ./test.sh
#  REQUIREMENTS:  bash, timeout
#         NOTES:  ---
#        AUTHOR:  Wenping Guo <ybyygu@gmail.com>
#       LICENCE:  GPL version 2 or upper
#       CREATED:  <2017-10-13 Fri 15:15>
#       UPDATED:  <2017-10-31 Tue 16:22>
#===============================================================================#
# 787fb596-b93f-4ed0-957c-853984dfd949 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::1c0957f3-e1b3-4378-97d8-afe05d644761][1c0957f3-e1b3-4378-97d8-afe05d644761]]
killtree() {
    echo "alive processes:"
    pstree -pal $$
    echo 'please wait ...'

    # kill child process tree
    kill -- $(ps -s $1 -o pid=)

    echo "subprocesses were terminated."
    pstree -pal $$
    exit 1
}

# kill tree by parent process id
kill_by_ppid() {
    pstree -pal $1
    kill $(/usr/bin/pstree -pl $1 | grep "([[:digit:]]*)" -o |tr -d '()')
    pstree -pal $1
}
# 1c0957f3-e1b3-4378-97d8-afe05d644761 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::d2799e53-2419-43bb-8492-9a7bfa0b8b48][d2799e53-2419-43bb-8492-9a7bfa0b8b48]]
# submit jobs in background
submit() {
    echo -n 'submitting...'
    setsid $* &
    # register trap
    trap "killtree $!" SIGINT SIGTERM
    echo 'submitted.'
}

# show helpful information
info() {
    echo "press ctrl-c to stop"
}

# run directly
run(){
    submit $*
    info
    # wait background jobs
    wait
    echo "main process $$: done"
}
# d2799e53-2419-43bb-8492-9a7bfa0b8b48 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::244c3dd5-b6e5-4ad2-9262-57fe5d33606a][244c3dd5-b6e5-4ad2-9262-57fe5d33606a]]
usage() {
    echo "Usage:"
    echo "$ $0 ./test.sh          run test.sh"
    echo "$ $0 -t 5h ./test.sh    set time limit as 5 hours (time unit: s, m, h)"
    echo "$ $0 -k 1090"           kill all child processes of 1090
    exit 1
}

while getopts ":ht:k:" opt; do
    case ${opt} in
        t  )
            TIMEOUT=$OPTARG
            echo "max allowed running time: $TIMEOUT"
            ;;
        k  )
            PPID_KILL=$OPTARG
            ;;
        h | *)
            usage
            ;;
    esac
done
shift $((OPTIND -1))
# 244c3dd5-b6e5-4ad2-9262-57fe5d33606a ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::414c2ffd-fb9a-4261-a2b1-b660cad10a4f][414c2ffd-fb9a-4261-a2b1-b660cad10a4f]]
# run will a time limit
if [ ! -z $TIMEOUT ]; then
    echo "time limit: $TIMEOUT"
    # call self with external timeout command
    timeout -s SIGINT --foreground $TIMEOUT $0 $@
elif [ ! -z $PPID_KILL ]; then
    kill_by_ppid $PPID_KILL
else
    if [ $# -lt 1 ]; then
        usage
    else
        run $*
    fi
fi
# 414c2ffd-fb9a-4261-a2b1-b660cad10a4f ends here
