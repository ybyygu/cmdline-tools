#! /usr/bin/env python3
# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::c746b489-f458-4711-bc1b-54e2ba4bcb74][c746b489-f458-4711-bc1b-54e2ba4bcb74]]
# -*- coding: utf-8 -*-
#==============================================================================#
#   DESCRIPTION:  files history manager
#
#       OPTIONS:  ---
#  REQUIREMENTS:  python-gobject (gi)
#         NOTES:  edit ~/.gtkrc-2.0 and appended below lines:
#                  gtk-recent-files-max-age=-1
#        AUTHOR:  Wenping Guo (ybyygu)
#         EMAIL:  winpng@gmail.com
#       LICENCE:  GPL version 2 or upper
#       CREATED:  <2010-08-31 Tue 19:41>
#       UPDATED:  <2017-10-30 Mon 14:21>
#==============================================================================#

__VERSION__ = '0.6.r13'
__UPDATED__ = '2012-12-25 20:12:51'
# c746b489-f458-4711-bc1b-54e2ba4bcb74 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::3445f232-8cb0-4b65-b845-5e9ea71debe4][3445f232-8cb0-4b65-b845-5e9ea71debe4]]
import sys

# standard modules first
sys.path.reverse()

import os
import time
import operator
import urllib
import subprocess
from collections import defaultdict

# import gnomevfs
import gio
import gtk
import pygtk
pygtk.require('2.0')

# I use gnome desktop
XDG_OPENER = "gnome-open"
#XDG_OPENER = "kde-open"
#XDG_OPENER = "xdg-open"

from guts import setup_logger
log = setup_logger("RFCommander")
# 3445f232-8cb0-4b65-b845-5e9ea71debe4 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::5cb05c33-da9d-4efa-84d2-4e9c39eebcc2][5cb05c33-da9d-4efa-84d2-4e9c39eebcc2]]
class RFCommander(object):
    """ Recent-used Files Commander """

    def __init__(self):
        self._manager = gtk.recent_manager_get_default()
        self._items = []
        # open parent directory
        self.open_directory = False
        self.open_with = XDG_OPENER

    def clean_items(self, not_local=True, not_exist=True):
        """ remove unnecessary items from the list """
        for item in self._manager.get_items():
            uri = item.get_uri()
            path = item.get_uri_display()
            if not item.is_local():
                if not_local:
                    self._manager.remove_item(uri)
                    log.info("removed not local item: %s" % path)
                else:
                    log.debug("ignore not local item: %s" % path)
                continue
            if not item.exists():
                if not_exist:
                    self._manager.remove_item(uri)
                    log.info("removed not exists item: %s" % path)
                else:
                    log.debug("ignore not exists item: %s" % path)


    def load_items(self, sticked=False, criteria={}):
        """
        select items which meet the criteria
        @sticked: load sticked items (remembered)
        """
        items = []

        if sticked:
            log.info("Sticked")
            criteria = {"apps": "Sticked"}

        def summary(item, term):
            actions = {
                "name": item.get_short_name,
                "uri": item.get_uri,
                "path": item.get_uri_display,
                "apps": item.get_applications,
                "applications": item.get_applications,
                "mime": item.get_mime_type,
                "visited":item.get_visited
                }
            result = actions[term]()
            if type(result) == type([]):
                result = ", ".join(result)
            # log.debug("%s" % result)
            return result

        for item in self._manager.get_items():
            if not criteria:
                items.append(item)
                continue
            for key, value in criteria.items():
                text = summary(item, key)
                if value.lower() not in text.lower():
                    break
            else:
                items.append(item)
        self._items = items
        log.info("Loaded %s items." % (len(self._items)))
        self._sort_items()

    def get_files(self, selections):
        """
        selections are numbers counting from 1
        """
        log.info("get files numbering as %s" % ",".join([str(num) for num in selections]))
        items = [self._items[num-1] for num in selections]
        files = []
        for item in items:
            files.append(item.get_uri())
        return files

    def print_files(self, files, makelinks=False):
        """ print item information """

        from unidecode import unidecode

        for uri in files:
            if self._manager.has_item(uri):
                item = self._manager.lookup_item(uri)
                path = item.get_uri_display()
            else:
                path = urllib.unquote(uri.encode("utf-8"))
            if makelinks:
                safename = unidecode(os.path.basename(path).decode('utf-8'))
                dst = os.path.join(os.getcwd(), safename)
                src = path
                os.symlink(src, dst)
            print("%s" % path)


    def summary_items(self, keys=["name"], key_delimiter="\t", item_delimiter="\n"):
        # width for dir output
        dir_width = 16

        def epoch_convert(epoch):
            return time.strftime("%Y-%m-%d %H:%M", time.localtime(epoch))

        def summary(item, key):
            actions = {
                "name": item.get_short_name,
                "uri": item.get_uri,
                "path": item.get_uri_display,
                "apps": item.get_applications,
                "applications": item.get_applications,
                "mime": item.get_mime_type,
                "visited": item.get_visited,
                "modified": item.get_modified,
                "added": item.get_added
                }
            if key in ("visited", "modified", "added"):
                return epoch_convert(actions[key]())
            if key == "path":
                home = os.path.expanduser("~")
                path = actions[key]()
                if path.startswith(home):
                    return path.replace(home, "~")
                return path
            if key == "info":
                uri = item.get_uri_display()
                if not uri:
                    print(item.get_uri())
                    return ""
                path = os.path.basename(os.path.dirname(uri))
                path = path.decode("utf8")
                _path = path.encode("gb18030")
                if len(_path) < dir_width:
                    path = _path.ljust(dir_width).decode("gb18030").encode("utf8")
                else:
                    for w in range(dir_width-3, 0, -1):
                        p = path[:w]
                        if len(p.encode("gb18030")) <= dir_width-3:
                            p += "."*(dir_width - len(p.encode("gb18030")))
                            path = p.encode("utf8")
                            break
                    else:
                        path = path[:dir_width-3] + "..."
                        path = path.encode("utf8")
                name = item.get_short_name()
                return "%s / %s" % (path, name)
            return "%s" % actions[key]()

        results = []
        i = 1
        items = self._items
        for item in items:
            text = [summary(item, key) for key in keys]
            results.append("%3i:\t" % i)
            results.append(key_delimiter.join(text))
            if item_delimiter:
                results.append(item_delimiter)
            i += 1
        # print results
        print("".join(results))

    def _sort_items(self):
        def sort_dwim(item):
            return max((item.get_visited(), item.get_modified(), item.get_added()))

        self._items.sort(key=sort_dwim, reverse=True)

    def open_item(self, uri):
        """
        open item with self.open_with
        """
        if self._manager.has_item(uri):
            item = self._manager.lookup_item(uri)
            path = item.get_uri_display()
        else:
            path = urllib.unquote(uri.encode("utf-8"))
        log.debug("%s" % path)
        if self.open_directory:
            path = os.path.dirname(path)
            uri = path_to_uri(path)
        args = ["%s" % self.open_with, path]
        log.info("%s -> %s" % (self.open_with, path))
        #subprocess.call(args)
        subprocess.Popen(args)

        self.add_files([uri])

    def forget_files(self, files):
        """ forget the files, i.e, remove the item from recently-used list """
        count = 0
        for uri in files:
            if self._manager.has_item(uri):
                item = self._manager.lookup_item(uri)
                log.info("item removed: %s" % item.get_short_name())
                self._manager.remove_item(uri)
                count += 1
        return count

    def add_files(self, files, app_name="RFManager"):
        """
        added files into recently-used list
        """

        for uri in files:
            f = gio.File(uri)
            f_info = f.query_info('standard::content-type')
            mime_type = f_info.get_content_type()
            self._manager.add_full(uri, recent_data={"mime_type": mime_type,
                                                     "app_name": app_name,
                                                     "app_exec": "/usr/bin/xdg-open"}
                                   )

    def remember_files(self, files):
        """ use a special app_name """

        self.add_files(files, app_name="Sticked")

def path_to_uri(path):
    path = os.path.abspath(path)
    return  "file://" + urllib.quote(path.encode('utf-8'))
# 5cb05c33-da9d-4efa-84d2-4e9c39eebcc2 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::6910c95c-a400-4681-8d79-5d90803f286a][6910c95c-a400-4681-8d79-5d90803f286a]]
def main(argv=None):
    """
    main story starts from here
    """
    import optparse

    if argv == None: argv = sys.argv

    # {{{ parsing cmdline
    cmdl_usage = 'usage: %prog [options]... [FILE]...'
    cmdl_version = "%prog " + __VERSION__
    cmdl_parser = optparse.OptionParser(usage=cmdl_usage, \
                                        version=cmdl_version, \
                    conflict_handler='resolve')
    cmdl_parser.add_option('-l', '--list',
                           action='store_true',
                           dest='list',
                           help='Show a list of recent files history.')
    cmdl_parser.add_option('-c', '--clean',
                           action='store_true',
                           dest='clean',
                           help='Clean unavailable items from list.')
    cmdl_parser.add_option('-d', '--directory',
                           action='store_true',
                           dest='directory',
                           help='Try to open parent directory instead of file.')
    cmdl_parser.add_option('-o', '--open',
                           dest='open',
                           action="store_true",
                           help='Open the selected file with xdg-open.')
    cmdl_parser.add_option('-p', '--print',
                           dest='_print',
                           action="store_true",
                           help='Print item information.')
    cmdl_parser.add_option('-L', '--link',
                           dest='link',
                           action="store_true",
                           help='make a soft link to the target.')
    cmdl_parser.add_option('-f', '--forget',
                           action="store_true",
                           dest='forget',
                           help='Forget about the selected file/files.')
    cmdl_parser.add_option('-r', '--recursive',
                           action="store_true",
                           dest='recursive',
                           help='Select files recursively into directory.')
    cmdl_parser.add_option('-m', '--remember',
                           action="store_true",
                           dest='remember',
                           help='Store/remember about the selected file/files.')
    cmdl_parser.add_option('-s', '--sticked',
                           action='store_true',
                           dest='sticked',
                           help='Load sticked items from history.')
    cmdl_parser.add_option('-a', '--application',
                           dest='application',
                           help='Open item with specified application.')

    (cmdl_opts, cmdl_args) = cmdl_parser.parse_args()
    # }}}

    if cmdl_opts.open and cmdl_args and cmdl_args[0].startswith("http"):
        path = cmdl_args[0]
        args = ["%s" % XDG_OPENER, path]
        log.info("%s -> %s" % (XDG_OPENER, path))
        subprocess.call(args)
        return
    if cmdl_opts.open and cmdl_args and cmdl_args[0].startswith("zotero://"):
        path = cmdl_args[0]
        opener = "firefox"
        args = [opener, path]
        log.info("%s -> %s" % (opener, path))
        subprocess.call(args)
        return


    commander = RFCommander()
    commander.load_items(cmdl_opts.sticked)

    if cmdl_opts.clean:
        commander.clean_items()
        return

    if cmdl_opts.list or cmdl_opts.sticked and len(cmdl_args) < 1:
        commander.summary_items(keys=["info"])
        return

    if len(cmdl_args) < 1:
        cmdl_parser.print_help()
        return

    if cmdl_opts.directory:
        commander.open_directory = True

    if cmdl_opts.application:
        commander.open_with = cmdl_opts.application

    # get selected file/files from cmdl_args
    files = set([])
    for obj in cmdl_args:
        if os.path.exists(obj):
            if cmdl_opts.recursive and os.path.isdir(obj):
                print("Recursively into direcotry %s:" % obj)
                for (_root, _subdirs, _files) in os.walk(obj):
                    for f in _files:
                        path = os.path.join(_root, f)
                        uri = path_to_uri(path)
                        files.add(uri)
                    for d in _subdirs:
                        path = os.path.join(_root, d)
                        uri = path_to_uri(path)
                        files.add(uri)
            else:
                uri = path_to_uri(obj)
                print("added from system: %s" % uri)
                files.add(uri)
        elif obj[0] in "123456789":
            from guts import parse_abbreviated_numbers

            numbers = parse_abbreviated_numbers(obj)
            for uri in commander.get_files(numbers):
                print("added from history: %s" % uri)
                files.add(uri)

    if not files:
        print("No files to operate on!")
        return

    if cmdl_opts.open:
        if len(files) > 1:
            print("Can open only one file at a time.")
            return
        commander.open_item(files.pop())
        return

    if cmdl_opts._print:
        commander.print_files(files)
        return

    if cmdl_opts.link:
        commander.print_files(files, makelinks=True)
        return

    if cmdl_opts.forget:
        count = commander.forget_files(files)
        print("Forgot %s file/files." % count)
        return

    if cmdl_opts.remember:
        commander.remember_files(files)
        if len(files) == 1:
            print("Remembered 1 file.")
        else:
            print("Remembered %s files." % len(files))
        return

if __name__ == '__main__':
    main()
# 6910c95c-a400-4681-8d79-5d90803f286a ends here
