#! /usr/bin/env python3
# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::2ef1c833-f2a8-4e14-89c1-44378e3e41f9][2ef1c833-f2a8-4e14-89c1-44378e3e41f9]]
# -*- coding: utf-8 -*-
#====================================================================#
# DESCRIPTION:  encode files into ascii string for easy copy and paste
#               through terminal interface; the encoded string can be
#               also decoded with "-d" option
#       OPTIONS:  ---
#  REQUIREMENTS:  python
#         NOTES:  ---
#        AUTHOR:  Wenping Guo (ybyygu)
#         EMAIL:  winpng@gmail.com
#       LICENCE:  GPL version 2 or upper
#       CREATED:  <2013-05-18 Sat 13:05>
#       UPDATED:  <2017-10-30 Mon 14:20>
#====================================================================#

__VERSION__ = '0.2'
__UPDATED__ = '2013-12-03 10:51:45 ybyygu'
# 2ef1c833-f2a8-4e14-89c1-44378e3e41f9 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::561f42cf-6c92-49c4-bb66-b3d711e83eaf][561f42cf-6c92-49c4-bb66-b3d711e83eaf]]
import os
import sys
import base64
import tarfile
import zipfile
import StringIO
# 561f42cf-6c92-49c4-bb66-b3d711e83eaf ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::19e2af6e-8f3e-4787-ba62-37d6463c9e42][19e2af6e-8f3e-4787-ba62-37d6463c9e42]]
def do_encode(files):
    """
    add files into zip archive and encode binary content into base64 string
    """

    zip_stream = StringIO.StringIO()
    z = zipfile.ZipFile(zip_stream, 'w')

    for afile in files:
        bfile = os.path.basename(afile)
        # use short name
        # print(afile, bfile)
        z.write(afile, bfile)
    z.close()

    zip_stream.seek(0)
    text = base64.b64encode(zip_stream.read())
    # wrap the long string into multiple lines
    # 76 is the default value in the separate program 'base64'
    # another way is to use email.base64MIME.encode
    #
    # textwrap.fill is too slow
    # return textwrap.fill(text, 76)
    lines = []
    m = int(len(text) / 76.0)
    for i in range(m):
        lines.append(text[i*76:(i+1)*76])
    lines.append(text[m*76:])
    return '\n'.join(lines)

def do_decode(data=None):
    """
    data should be base64 encoded string

    this function will decode it and then treat it as a zip archive and extract all files in
    """

    # print(os.getcwd())

    # read base64 encoded string in multiple lines
    if data is None:
        lines = sys.stdin.readlines()
        data = "".join(lines)

    # extract into current directory
    data = StringIO.StringIO(base64.b64decode(data))
    z = zipfile.ZipFile(data)
    z.extractall()
    print("Extracted {:} files:".format(len(z.namelist())))
    for name in z.namelist():
        print("..{:}".format(name))

def copy_to_clipboard(content):
    import gtk
    import subprocess

    print(content)

    # 2013-11-10: doesnot work since gnome update
    # clipboard = gtk.Clipboard(gtk.gdk.display_get_default(), selection="CLIPBOARD")
    # clipboard.set_text(content)
    # clipboard.store()
    p = subprocess.Popen(['xclip', '-selection', 'clipboard'], stdin=subprocess.PIPE)
    stdout, stderr = p.communicate(content)
    if stdout: print stdout
    if stderr: print stderr

    # 2013-12-03: also save to screen exchange file
    open("/tmp/screen-exchange", "w").write(content)


def get_data_from_clipboard():
    import gtk

    clipboard = gtk.Clipboard(gtk.gdk.display_get_default(), selection="CLIPBOARD")
    text = clipboard.wait_for_text()
    return text
# 19e2af6e-8f3e-4787-ba62-37d6463c9e42 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::5f0048e8-dfea-4ced-a563-f6b200edcc2b][5f0048e8-dfea-4ced-a563-f6b200edcc2b]]
def main(argv=None):
    import optparse

    if argv == None: argv = sys.argv

    # parsing cmdline
    cmdl_usage = 'encode and decode files for remote copy & paste through plain text (base64 MIME)\nusage: %prog [options]...'
    cmdl_version = "%prog " + __VERSION__
    cmdl_parser = optparse.OptionParser(usage=cmdl_usage, \
                                        version=cmdl_version, \
                                        conflict_handler='resolve')
    cmdl_parser.add_option('-h', '--help',
                           action='help',
                           help='print this help text and exit')
    cmdl_parser.add_option('-v', '--version',
                           action='version',
                           help='print program version and exit')
    cmdl_parser.add_option('-d', '--decode',
                           action='store_true',
                           dest='decode',
                           default=False,
                           help='decode encoded files')
    cmdl_parser.add_option('-c', '--clipboard',
                           action='store_true',
                           dest='clipboard',
                           default=False,
                           help='get data from clipboard or save data to clipboard (for X system only)')

    (cmdl_opts, cmdl_args) = cmdl_parser.parse_args()

    if cmdl_opts.decode:
        if cmdl_opts.clipboard:
            data = get_data_from_clipboard()
            do_decode(data)
        else:
            do_decode()
    elif len(cmdl_args) > 0:
        content = do_encode(cmdl_args)
        if cmdl_opts.clipboard:
            copy_to_clipboard(content)
            print("saved to clipboard")
        else:
            print(content)
    else:
        cmdl_parser.print_help();

if __name__ == '__main__':
    main()
# 5f0048e8-dfea-4ced-a563-f6b200edcc2b ends here
