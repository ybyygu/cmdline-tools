#! /usr/bin/env bash
# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::4466a480-41c7-4de5-8974-c4a0662ae660][4466a480-41c7-4de5-8974-c4a0662ae660]]
xprop _NET_WM_PID | awk '{print $3}'
# 4466a480-41c7-4de5-8974-c4a0662ae660 ends here
