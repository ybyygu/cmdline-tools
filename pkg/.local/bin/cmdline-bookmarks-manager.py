#! /usr/bin/env python
# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::6acd9cb5-8a07-4aea-8d5b-827a443de91a][6acd9cb5-8a07-4aea-8d5b-827a443de91a]]
# -*- coding: utf-8 -*-
#==============================================================================#
#   DESCRIPTION:  command line bookmarks manager
#
#       OPTIONS:  ---
#  REQUIREMENTS:  python2
#         NOTES:  ---
#        AUTHOR:  Wenping Guo <ybyygu@gmail.com>
#       LICENCE:  GPL version 3
#       CREATED:  <2009-10-27 Tue 10:21>
#       UPDATED:  <2017-10-30 Mon 14:20>
#==============================================================================#
# 6acd9cb5-8a07-4aea-8d5b-827a443de91a ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::a39ee01f-7446-4679-b997-052d3433d386][a39ee01f-7446-4679-b997-052d3433d386]]
from __future__ import print_function
import os
import sys

from urllib.parse import unquote

__VERSION__ = "0.6"

# nautilus bookmarks
bookmark_gtk = "~/.config/gtk-3.0/bookmarks"
# cmdline bookmarks
bookmark_cmd = "~/.cmd_bookmarks"
# cmdline history entries
history_cmd = "~/.cmd_history"
# the maximum stored history entries
max_history_entries = 15
# bookmark_xbel = "~/.recently-used.xbel"
# alias to current python command
bookmark_cmd_in_shell = "cb"

bookmark_gtk = os.path.expanduser(bookmark_gtk)
bookmark_cmd = os.path.expanduser(bookmark_cmd)
history_cmd = os.path.expanduser(history_cmd)
# bookmark_xbel = os.path.expanduser(bookmark_xbel)
# a39ee01f-7446-4679-b997-052d3433d386 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::a2ee720c-ff96-4cb8-a451-6fac6ba1b995][a2ee720c-ff96-4cb8-a451-6fac6ba1b995]]
def escape(path):
    if " " in path:
        path = "%s" % (path.replace(" ", "\ "))
    if "(" in path:
        path = "%s" % (path.replace("(", "\("))
    if ")" in path:
        path = "%s" % (path.replace(")", "\)"))

    return path

def unescape(path):
    if "\ " in path:
        path = "%s" % (path.replace("\ ", " "))
    if "\(" in path:
        path = "%s" % (path.replace("\(", "("))
    if "\)" in path:
        path = "%s" % (path.replace("\)", ")"))

    return path


def load_bookmarks(source=None):
    """ load bookmarks from disk """

    bookmarks = {}

    # load options from history entries
    if source == ".":
        if not os.path.exists(history_cmd):
            return {}

        i = 0
        lines = open(history_cmd).readlines()
        lines.reverse()
        for path in lines:
            path = path.strip()
            if not path or i >= max_history_entries or not os.path.exists(path):
                continue
            alias = "{0:0{2}} {1}".format(i, path, len(str(max_history_entries)))
            # print path
            bookmarks[alias] = path
            i += 1

        return bookmarks

    # load options from source (directory?)
    elif source is not None:
        bookmarks = load_bookmarks()
        # take as bookmark alias
        sbookmarks = {}
        if source in bookmarks:
            source = bookmarks[source]
        try:
            dirs = os.listdir(source)
        except OSError:
            return {}

        for obj in os.listdir(source):
            # skip hidden directories
            if obj.startswith("."):
                continue
            path = os.path.join(source, obj)
            if os.path.isdir(path):
                path = os.path.abspath(path)
                sbookmarks[obj] = path
        return sbookmarks

    # load gtk bookmarks used in nautilus
    with open(bookmark_gtk) as fbookmark:
        for line in fbookmark:
            if line.startswith("file://"):
                attrs = line.strip().split()
                if 1 <= len(attrs) <= 2:
                    url = unquote(attrs[0][7:])
                    if len(attrs) == 1:
                        alias = os.path.basename(url)
                    else:
                        alias = attrs[1]
                    bookmarks[alias] = url

    # load user defined bookmarks
    with open(bookmark_cmd) as fbookmark:
        for line in fbookmark:
            attrs = line.strip().split()
            if len(attrs) == 2:
                alias = attrs[0]
                url = attrs[1]
                bookmarks[alias] = url
    return bookmarks

def edit_bookmarks():
    """ TODO: FIXME: edit bookmarks in ncurses interface """
    import curses

    myscreen = curses.initscr()
    myscreen.keypad(1)
    curses.curs_set(0)
    curses.noecho()
    curses.cbreak()
    myscreen.border(0)
    myscreen.refresh()

    x = 0
    while x != ord("q"):
        myscreen.refresh()

        bookmarks = load_bookmarks()

        num = max([len(key) for key in bookmarks.keys()])
        i = 0
        for key, value in bookmarks.items():
            i += 1
            myscreen.addstr(i, 2, key)
            myscreen.addstr(i, num+5, value)

        x = myscreen.getch()
        if x == ord("q"):
            break
        elif x ==curses.KEY_UP:
            myscreen.addstr(10, 10, "good")

    curses.nocbreak()
    curses.echo()
    myscreen.keypad(0)
    curses.endwin()

def search_bookmarks(bookmarks, pattern=None, usepath=False, fuzzy=True):
    if pattern is not None:
        pattern = unescape(pattern)

    keys = []
    for key in bookmarks:
        if pattern == key:
            keys = [key]
            break
        if fuzzy:
            if pattern in key:
                keys.append(key)
        else:
            if pattern is not None and key.startswith(pattern):
                keys.append(key)
    # print(keys)
    if usepath:
        return [bookmarks[key] for key in keys]
    else:
        return keys

def print_bookmarks(resp):
    if len(resp) == 1:
        print(escape(resp[0]))
        return 0

    for p in resp:
        print(escape(p), file=sys.stderr)

    return -1

def cmdl_bookmark_completion(cmd, cur, pre):
    """
    for bash cmdline completion

    Arguments:
    - `cmd`: the cmd used in bash shell
    - `cur`: current user input
    - `pre`: previous user input
    """

    if cmd != bookmark_cmd_in_shell:
        return

    if pre == ".":
        bookmarks = load_bookmarks(source=".")
        resp = search_bookmarks(bookmarks, cur)
        for p in resp:
            print(escape(p))
    elif pre == bookmark_cmd_in_shell:
        bookmarks = load_bookmarks(source=None)
    else:
        bookmarks = load_bookmarks(source=pre)
    resp = search_bookmarks(bookmarks, cur)
    for p in resp:
        print(escape(p))
# a2ee720c-ff96-4cb8-a451-6fac6ba1b995 ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::aabe3607-225b-4826-a3e7-3dd2eb23a5b3][aabe3607-225b-4826-a3e7-3dd2eb23a5b3]]
def main(argv=None):
    """ main story starts from here """
    import optparse

    if argv == None: argv = sys.argv

    # complete -C this_python_cmd cmd
    if len(argv) == 4 and not argv[1].startswith("-"):
        cmd = argv[1]
        cur = argv[2]
        pre = argv[3]
        cmdl_bookmark_completion(cmd, cur, pre)
        return 0

    # parsing cmdline
    cmdl_usage = 'usage: %prog [options]...[queue_id]'
    cmdl_version = "%prog " + "%s" % __VERSION__
    cmdl_parser = optparse.OptionParser(usage=cmdl_usage, \
                                        version=cmdl_version, \
                    conflict_handler='resolve')
    cmdl_parser.add_option('-h', '--help', action='help',
                           help='print this help text and exit')
    cmdl_parser.add_option('-v', '--version', action='version',
                           help='print program version and exit')
    cmdl_parser.add_option('-q', '--pattern', \
                           dest='pattern',
                           default=None,
                           help='search pattern')
    cmdl_parser.add_option('-e', '--edit', dest='edit',
                           action="store_true",
                           default=False,
                           help='edit command line bookmarks')
    (cmdl_opts, cmdl_args) = cmdl_parser.parse_args()

    # edit bookmarks in ncurse interface
    if cmdl_opts.edit:
        return edit_bookmarks()

    usepath = False
    fuzzy = False
    pattern = cmdl_opts.pattern
    if len(cmdl_args) == 1:
        source = cmdl_args[0]
        usepath = True
    else:
        usepath = True
        source = None

    bookmarks = load_bookmarks(source)
    if not pattern:
        for key in bookmarks:
            print(key)
    # print (bookmarks)
    resp = search_bookmarks(bookmarks, pattern, usepath=usepath, fuzzy=fuzzy)
    return print_bookmarks(resp)

if __name__ == '__main__':
    ret_code = main()
    sys.exit(ret_code)
# aabe3607-225b-4826-a3e7-3dd2eb23a5b3 ends here
