#! /usr/bin/env bash
# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::6a38ec37-e685-4d2a-af88-32dfa0a3c45d][6a38ec37-e685-4d2a-af88-32dfa0a3c45d]]
function dircmp() {
    rsync --recursive --delete --links --checksum --verbose --itemize-changes --dry-run "$1" "$2"
}
# 6a38ec37-e685-4d2a-af88-32dfa0a3c45d ends here

# [[file:~/Workspace/Programming/cmdline-tools/cmdline-tools.note::e82296a3-3e21-4812-9715-a25b68e0d211][e82296a3-3e21-4812-9715-a25b68e0d211]]
usage() {
    echo "show changes between two directories using rsync dry-run"
    echo
    echo "Usage:"
    echo "> $0 dir1/ dir2/"

    echo "Note: the slash after dir1 or dir2 is important"
}

if [[ $# -ge 2 ]]; then
    dircmp $@
else
    usage
    exit 1
fi
# e82296a3-3e21-4812-9715-a25b68e0d211 ends here
